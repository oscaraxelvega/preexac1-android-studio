package com.example.preexamenc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaActivity extends AppCompatActivity {
    private TextView lblTrabajador, lblRecibo, lblSubtotal, lblImpuestos, lblTotal;
    private EditText txtHoras, txtHorasExtra;
    private RadioButton rdbAuxiliar, rdbAlbanil, rdbIngobra;
    private Button btnRregresar, btnLimpiar, btnCalcular;
    private ReciboNomina recibo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        initComponents();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtHoras.getText().toString().matches("") || txtHorasExtra.getText().toString().matches("")){
                    Toast.makeText(ReciboNominaActivity.this, "Faltaron datos por ingresar", Toast.LENGTH_SHORT).show();
                } else{
                    recibo.setPuesto(rdbAuxiliar.isChecked() ? 1 : rdbAlbanil.isChecked() ? 2 : rdbIngobra.isChecked() ? 3 : -1);
                    recibo.setHorasTrabNormal(Float.parseFloat(txtHoras.getText().toString()));
                    recibo.setHorasTrabExtra(Float.parseFloat(txtHorasExtra.getText().toString()));

                    float subtotal = recibo.calcularSubtotal();
                    float impuesto = recibo.calcularImpuessto();
                    float total = recibo.calularTotal();

                    lblSubtotal.setText(String.valueOf("Subtotal: $" + subtotal));
                    lblImpuestos.setText(String.valueOf("Impuestos: $" + impuesto));
                    lblTotal.setText(String.valueOf("Total a pagar: $" + total));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblSubtotal.setText(String.valueOf("Subtotal: $"));
                lblImpuestos.setText(String.valueOf("Impuestos: $"));
                lblTotal.setText(String.valueOf("Total a pagar: $"));
                rdbAuxiliar.setChecked(true);
                txtHoras.setText("");
                txtHorasExtra.setText("");
            }
        });

        btnRregresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void initComponents(){
        txtHoras = (EditText) findViewById(R.id.txtHoras);
        txtHorasExtra = (EditText) findViewById(R.id.txtHorasExtra);
        lblTrabajador = (TextView) findViewById(R.id.lblTrabajador);
        lblImpuestos = (TextView) findViewById(R.id.lblImpuestos);
        lblRecibo = (TextView) findViewById(R.id.lblRecibo);
        lblSubtotal = (TextView) findViewById(R.id.lblSubtotal);
        lblTotal = (TextView) findViewById(R.id.lblTotal);
        rdbAlbanil = (RadioButton) findViewById(R.id.rdbAlbanil);
        rdbAuxiliar = (RadioButton) findViewById(R.id.rdbAuxiliar);
        rdbIngobra = (RadioButton) findViewById(R.id.rdbIngobra);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRregresar = (Button) findViewById(R.id.btnRegresar);
        recibo = new ReciboNomina();
        lblRecibo.setText("Recibo: "+String.valueOf(recibo.generarId()));
        Bundle datos =getIntent().getExtras();
        String nombre = datos.getString("trabajador");
        lblTrabajador.setText("Trabajador: " + nombre);
        rdbAuxiliar.setChecked(true);
    }
}