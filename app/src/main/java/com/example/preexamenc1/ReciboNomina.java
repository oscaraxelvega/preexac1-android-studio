package com.example.preexamenc1;

import java.util.Random;

public class ReciboNomina {
    private int numRecibo, puesto;
    private String nombre;
    private float horasTrabNormal, horasTrabExtra, impuestoPorc;

    public ReciboNomina(int numRecibo, int puesto, String nombre, float horasTrabNormal, float horasTrabExtra, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.puesto = puesto;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtra = horasTrabExtra;
        this.impuestoPorc = impuestoPorc;
    }
    public ReciboNomina() {
        this.numRecibo = 0;
        this.puesto = 0;
        this.nombre = "";
        this.horasTrabNormal = 0.0f;
        this.horasTrabExtra = 0.0f;
        this.impuestoPorc = 0.16f;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtra() {
        return horasTrabExtra;
    }

    public void setHorasTrabExtra(float horasTrabExtra) {
        this.horasTrabExtra = horasTrabExtra;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float calcularSubtotal(){
        if (this.puesto == 1){
            return (200*this.horasTrabNormal) + (200*1.20f*2*this.horasTrabExtra);
        } else if (this.puesto == 2) {
            return (200*this.horasTrabNormal) + (200*1.50f*2*this.horasTrabExtra);
        } else if (this.puesto == 3) {
            return (200*this.horasTrabNormal) + (200*2*2*this.horasTrabExtra);
        } else {
            return -1;
        }
    }

    public float calcularImpuessto(){
        return calcularSubtotal()*impuestoPorc;
    }

    public float calularTotal(){
        return calcularSubtotal() - calcularImpuessto();
    }

    public int generarId() {
        Random r = new Random();
        return r.nextInt(1000);
    }
}
